*(Note from Rawrior: This repo is intended only as a 'mirror' of the KRust source code, as I had trouble finding the source code when following the URL in the article. Original KRust article can be found on [here on Arxiv.org](https://arxiv.org/abs/1804.10806). Any contact about KRust should be made to the original authors.)*

### KRust: A Formal Executable Semantics of Rust

This is the source code of KRust, an executable semantics of a subset of Rust using K framework.

### Requirements

- Make sure you have installed K framework 4.0.0 successfully. If not, download it [here](https://github.com/kframework/k-legacy/releases/tag/v4.0.0) and follow the installation [instructions](https://github.com/kframework/k5).
- KRust can be successfully run on OS X EI Capitan 10.11.6 and Ubuntu 12.04.5. Other systems with K framework 4.0.0 should run it successfully too.

### Download
[source code](krust.zip)

### Usage

Above all, kompile the source code using command:

```
kompile rs.k
```

Use krun command to run test cases. For instance:

```
kun tests/while.rs
```

### Project Structure

- **rs-semantics-\*.k** contains the specific semantics rules.
- **rs-syntax.k** only defines the syntax.
- **rs.k** wraps **rs-semantics-\*.k** and **rs-syntax.k**
- **tests** includes some Rust codes used to test all the rules. It also contains a folder called **passed-files-from-run-pass**. The codes in this folder are from Rust compiler's test suite which krust can also successfully pass. The **proofs** folder in **tests** is used for verification.
- **extended-float.k** is used to implement float numbers.

### License

See [LICENSE](LICENSE.txt)
