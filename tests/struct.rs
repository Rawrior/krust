struct Point {
    x:i32,
    y:i32
}
struct Test {
    a : i32,
}
// struct Test2 {
//     c : bool,
//     d : f64,
//     e : u32,
// }
fn main() {

    let mut p = Point {x:2,y:3};
    p.x = 100;
    let temp = p;
    println!("{}",temp.x);
    let t = Test{a:1};
    println!("{}", t.a);
    // p.x = 100;
    // p.y = 200;
    // let k = p.x;
    // let z = p.y;
    // println!("{},{}",k,z);


    //q.x = 9; // Wrong : q is moved
}
