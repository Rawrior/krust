struct Point {
    x : i32,
    y : i32,
}
fn main() {
    // declare struct instance
    let mut X  = Point {x:2, y:3};
    let mut Y = Point {x:100,y:100};
    Y.x = 0;
    let mut Z = Y;
    println!("{},{}", Z.x, Z.y);
    Z = X;
    println!("{},{}", Z.x, Z.y);
    // Y.y = 9; Error! Y is moved!
    // X.x = 100; Error! X is moved!

}