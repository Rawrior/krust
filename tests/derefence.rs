fn main() {
    let mut y = 1;
    
    {
        let mut x;
        x = &mut y;
        *x = 9;
    }
    println!("y = {}", y);
}