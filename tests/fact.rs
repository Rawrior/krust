fn f(x: isize) -> isize {

    println!("{}", x);
    if x == 1 {

        return 1;
    } else {

        let y: isize = x * f(x - 1);
        println!("{}", y);
        return y;
    }
}

fn main() {
    assert_eq!(f(1), 1);
    assert!(f(3) == 6);
    assert_eq!(f(5), 120);

}
