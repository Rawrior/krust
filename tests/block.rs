fn main() {
    let mut x = 0;
    {
        let y = 1;
        {
            let z = x;
            println!("{:?}", x);
            x = 9;
        }
    }
    println!("{}", x);
}