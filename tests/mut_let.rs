// 当变量没有赋值，或者是mutable的时候才可以重新assign
fn main() {
    // mutable variables declararion
    // 1
    let mut x ;
    x = 100;
    x = 10; //re-assign
    // 2
    let mut y: i32 ;
    y = 1;
    y = 200; //re-assign
    // y = false; wrong because type inconsistant
    // 3
    let mut a = 300;
    a = a*2;
    // 4 
    let mut b: i32 = 400;
    b = b + 4/2;
    println!("{},{}",x,y);
}
