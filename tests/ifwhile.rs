fn main() {

    let mut y = 0;
    if 1 < 2 {
        y = 2;
    } else {
        y = 3;
    }
    println!("{}", y);

    let mut x: i32 = 100;
    let mut sum: i32 = 0;
    while x > 0 {
        sum = sum + x;
        x -= 1;
    }
    println!("{}", sum);

}
