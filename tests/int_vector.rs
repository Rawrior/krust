fn main(){
    let mut v = vec![1,3,5,7,9];
    let x = v[0];
    println!("{}", x); // OK
    // // println!("{}", v[5]); out of index
    v.push(100); //OK
    println!("{}", v[5]);//OK
    v.pop();
    println!("{}", v[4]);
    //println!("{}", v[5]);// out of index
    let mut z = vec![1,2,3];
    z = v;
    println!("{}", z[3]);
    //println!("{}", v[0]);
    let mut a : Vec<i32> = vec![0,9,8];
    a = vec![120,119,911];
    println!("{}", a[2]);
}