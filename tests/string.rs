fn main() {
    let mut s = "Hi";
    assert_eq!("Hi", s);
    s = "world";
    println!("{}", s);
    let mut p: &str = "wang" ;
    println!("{}", p);
    let mut q:&'static str = "Elon";
    q = "SOS";
    println!("{}", q);
    let t = 'x';
    println!("{}", t);
}