#![allow(unused_variables)]
fn main() {
    // untyped declaration
    // 1
    let RR = 1;
    let RR = 2;
    println!("{}", RR);
    let t = {10}*3;
    println!("{}", t);
    let mut x = 0;
    assert_eq!(x, 0);
    //x = 1; // wrong. re-assgin immutable variable
    // 2
    let y: i32;
    y = 1;
    x += y;
    //y = 2; // wrong. re-assgin immutable variable
    // 3
    let a = 3;
    //a = 4; // wrong. re-assgin immutable variable
    // 4
    let b: i32  = x;
    //b = 5; // wrong. re-assgin immutable variable
    let z: isize = 9;
    //print(x,y,a,b,z);
    println!("z = {}", z);
    assert_eq!(2, 2);
    assert_eq!(1+1, 2);
    assert_eq!(x, 1);
    assert_eq!(1, y);
    assert_eq!(x+1, y+1);
    test_float();
    let mut c = 0;
    c += 2;
    println!("{}", c); // c = 2
    c -= 1; // c = 1 
    println!("{}", c);
    c *= 2; // c = 2;
    println!("{}", c);
    c /= 2; // c = 1;
    println!("{}", c);

    let mut p = c *= 10;
    assert_eq!(p, ());
    println!("{}", c);
    p = c -= 11;
    assert_eq!(p, ());
    println!("{}", c);
}
fn test_float(){
    let x = 2.13e1f64;
    let y = 2.13e1f32;
    let z = 1E-9;
    let q = 2.3e+8;
    println!("{}", -2.0*(2.1+9.8/7.1)-2.0*3.0);
    println!("{},{},{},{}", x,y, z, q);
    //assert_eq!(x, y);
    //assert_eq!(x+y, 4.2);
    println!("{}", x-y);
    println!("{}", x+y);
    println!("{}", x*y);
    println!("{}", x/y);
}