const FOO : i32 = 1;
static mut BAR : i32 = 10;
static COOL : f64 = 3.14;

fn main() {

    println!("{}", FOO);
    println!("{}", BAR);
    println!("{}", COOL);
}