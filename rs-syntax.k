/*
* Formalizing Rust using K framework
* Author: Jerry Wang
*/

require "extended-float.k"
module RS-SYNTAX

  imports FLOAT 

  syntax Id ::= "main"         [token] 
              | "panic!"         [token] 
              |  "push"       [token] 
              |  "pop"       [token]

  syntax Comment ::= "#!/usr/bin/env rustx"       [token]
  syntax Char ::= r"\\'[0-9A-Za-z]\\'"  [token]
           
  syntax Type ::= "i32"
                | "u32" 
                | "i64" 
                | "bool"
                | "isize"
                | "usize"
                | "f32"
                | "f64"          
                | "&str"
                | "&'static" "str" 
                | "&'static" "isize" 
                | "char"
                | "()"         [klabel('empty)]
                | "u8"
                | "i8"
                | "i16"
                | "u16"
                | "u64"
                | "&mut isize"
                | "&isize"
                | "*mut" Type     
                | "*const" Type   
                | "fn" "(" Types ")" "->" Type 
                | "Vec<isize>"
                | "Vec<i32>"

  syntax Types ::= List{Type,","}

  syntax ArrayType ::= "[" Type ";" Exp "]"  [strict(2),klabel('array)] // array: [i32;2]


  syntax DeclExp ::= "let" Id ";"            // let x;
                  |  "let" "mut" Id ";"      // let mut x;
                  | "let" Id "=" Exp ";"                      [strict(2)] // let x = 0;
                  | "let" Id "=" Ref Exp ";"
                  | "let" "mut" Id "=" Exp ";"                [strict(2)] // let mut x = 0;
                  | "let" "mut" Id "=" Ref Exp ";" 
                  | "let" Id "=" StructInstance ";"
                  | "let" "mut" Id "=" StructInstance ";"
                  | "let" "ref" Id "=" Exp ";"
                  | "let" "ref" "mut" Id "=" Exp ";"
                  
  syntax TypedId ::= Id ":" Type 
  syntax TypedIds ::= List{TypedId, ","}
  // typed declaration
  syntax TypedDecExp ::= "let" Id ":" Type ";"       // let x: i32;
                      |  "let" "mut" Id ":" Type ";" // let mut x: i32;
                      |  "let"  Id ":" Type "=" Ref Exp ";"
                      |  "let" "mut" Id ":" Type "=" Ref Exp ";" 
                      |  "let" Id ":" Type "=" Exp ";"        [strict(3)] // let x: i32 = 0;
                      |  "let" "mut" Id ":" Type "=" Exp ";"  [strict(3)] // let mut x: i32 = 0;

  
  syntax TypedArrayDec ::= "let" Id ":" ArrayType ";"       // let x : [i32;3];
                        |  "let" "mut" Id ":" ArrayType ";" // let mut x : [i32;3];
                        |  "let" Id ":" ArrayType "=" "[" Exps "]"  ";"    [strict(3)]   
                        |  "let" Id ":" ArrayType "=" "[" Exp ";" Exp "]"  ";" [strict(3,4)]  
                        |  "let" "mut" Id ":" ArrayType "=" "[" Exps "]"  ";"  [strict(3) ]
                        |  "let" "mut" Id ":" ArrayType "=" "[" Exp ";" Exp "]"  ";"  [strict(3,4)] 
  
  syntax UnTypedArrayDec ::=  "let" Id  "=" "[" Exps "]"  ";"   [strict(2) ] 
                        |  "let" Id  "=" "[" Exp ";" Exp "]"   ";" [strict(2,3)] 
                        |  "let" "mut" Id  "=" "[" Exps "]"  ";"  [strict(2)]
                        |  "let" "mut" Id  "=" "[" Exp ";" Exp "]"   ";"  [strict(2,3)] 
  
  syntax ArrayDec ::=  TypedArrayDec | UnTypedArrayDec
                   |  "static" Id ":" ArrayType "=" "[" Exps "]" ";"          [strict(3)]
                   |  "static" Id ":" ArrayType "=" "[" Exp ";" Exp "]"   ";" [strict(3,4)]
                   |  "const" Id ":" ArrayType "=" "[" Exps "]" ";"           [strict(3)]
                   |  "const" Id "=" "[" Exps "]" ";"                         [strict(2)]
                   |  "const" Id ":" ArrayType "=" "[" Exp ";" Exp "]"   ";"  [strict(2,3,4)]

  // 1.23f64 => K sees 1.23f, it sucks
  syntax ValWithType ::=   Float "f64"
                        |  Float "f32"
                        |  Int "isize"
                        |  Int "as usize"
                        |  Exp "as usize"   
                        |  Int "as isize"
                        |  Int "_isize"
                        |  Int "_usize"
                        |  Int "as (usize)"
                        |  Int "as (isize)"
  
  
  syntax Exp ::=   
                Int | Bool | Float | String  | Char 
               | Id
               | "()"
               | StructElementsAccess
               | "*"Id  // *x
               | "::"Id // ::x
               | ValWithType 
               |  StructValues
               |  "{" Exp "}"             [strict]
               | "(" Exp ")"              [bracket]
               | Id ".len()"
               > Exp "[" Exp "]"         [strict] // x[0]
               | "if" Exp "{" Exp "}" "else" "{" Exp "}" "[" Exp "]"           [strict(1,4)]
               > Exp "(" Exps ")"        [strict] // function(1,2,3)
               | Exp "(" Ref Exp ")"
               | "-" Exp                 [strict]
               >  Exp "*" Exp             [strict, left]              
               | Exp "/" Exp             [strict, left]               
               | Exp "%" Exp             [strict, left] 
               | Exp ">>" Exp            [strict]
               | Exp "<<" Exp             [strict]  
               | Exp "&" Exp             [strict] 
               | Exp "|" Exp             [strict] 
               > Exp "+" Exp              [strict, left]
               | Exp "-" Exp              [strict, left]
               > Exp "<" Exp             [strict, non-assoc]
               | Exp "<=" Exp            [strict, non-assoc]
               | Exp ">" Exp             [seqstrict, non-assoc]
               | Exp ">=" Exp            [strict, non-assoc]
               | Exp "==" Exp            [strict, non-assoc]
               | Exp "!=" Exp            [strict, non-assoc]
               > "!" Exp                 [strict]
               > Exp "&&" Exp            [strict(1), left]
               | Exp "||" Exp            [strict(1), left]
               
  syntax Body ::= Block | Exp         
  syntax  If ::=   "if"  Exp  Body "else" Body      [strict(1)]
              |    "if"  Exp  Body                   [strict(1)]
              |   "if" Exp Body "else" "if" Exp Body    [strict(1,3)]
              |   "if" Exp Body "else" "if" Exp Body "else" Body   [strict(1,3)]
              |   "if" Exp Body "else" "if" Exp Body "else" "if" Exp Body "else" Body [strict(1,3,5)]

  syntax  IfAndWhile ::=  If | While 

  syntax While ::=  "while"  Exp  Block 
                |   "while"  Exp  "{" AugAssign "}" 
                |   "loop" Block  

  syntax For ::= "for" Id "in" Exp ".." Exp Block   [strict(2,3)]
                         
               
               
  syntax AugAssign ::= Id "+=" Exp    [right]
                    |  Id "-=" Exp    [right]
                    |  Id "/=" Exp    [right]
                    |  Id "*=" Exp    [right]
                    |  Id "%=" Exp    [right]
                    |  "*"Id "+=" Exp    [right]
                    |  "*"Id "-=" Exp    [right]
                    |  "*"Id "/=" Exp    [right]
                    |  "*"Id "*=" Exp    [right]
                    |  "*"Id "%=" Exp    [right]

  syntax Ids  ::= List{Id,","}
  syntax Exps ::= List{Exp,","}                     [strict]  

  syntax Block ::= "{" "}"
                | "{" Stmts "}"
                | "{" Stmts Exp "}" 

                

  // variable assignment              
  syntax AssignStmt ::= Id "=" Exp ";"                    [strict(2)]      
                      | Id "="  Id "{" StructValues "}" ";"            
                      | Id "[" Exp "]" "=" Exp ";"        [strict(2,3)]        
                      | "*" Id "=" Exp ";"                [strict(2)]       
                      | StructElementsAccess "=" Exp ";"  [strict(2)]  
                      | Id "=" AugAssign ";"
                      | Id "=" Id "=" Exp ";"             [strict(3)]
                      | Id "="  "[" Exps "]" ";"          [strict(2)]  
                      | Id "=" "[" Exp ";" Int "]" ";"    [strict(2)]   

  // print 
  syntax PrintStmt ::= "print" "(" Exps ")" ";"      [strict]
                    |  "println!" "(" String ")" ";"
                    |  "println!" "(" String "," Exps ")" ";"   
                    >  "println!" "(" String "," "(" Exps ")" ")" ";"  [macro]
  // reference 
  syntax Ref ::=  "&"   [klabel('mut)]
              |  "&mut" [klabel('imut)]
            
  // reference statement
  syntax RefStmt ::= Id "=" Ref Exp ";"
  // Struct 
  syntax Struct ::= "struct" Id "{" TypedIds "}"       // struct Point {X:i32, Y:i32}
                 |  "struct" Id "{" Id ":" Type "," "}" // struct Point {X: i32,}
                 |  "struct" Id "{" Id ":" Type "," Id ":" Type "," "}" // struct Point {X: i32, Y:i32,}
                 | "pub" "struct" Id "{" TypedIds "}"       
                 | "pub" "struct" Id "{" Id ":" Type "," "}" 
                 | "pub" "struct" Id "{" "pub" Id ":" Type "," "pub" Id ":" Type "," "}"
                 | "pub" "struct" Id ";"
                 | "struct" Id ";"

  syntax StructValue ::= Id ":" Exp    [strict(2)]

  syntax StructValues ::= NeList{ StructValue , ","}
  syntax StructElementsAccess ::= Id "." Id
  syntax StructInstance ::= Id "{" StructValues "}"   [klabel('si)]
  syntax Attribute ::= "#" "[" MetaItem "]"
                    |  "#" "!" "[" MetaItem "]"
  
  syntax MetaItem ::= Id "=" String 
                   |  Id "(" MetaSeq ")"
                   |  Id
  
  syntax MetaSeq ::= Id
                  | List{MetaItem, ","}

  syntax ComplexExp ::= "vec!" "[" Exps "]"   [strict]
                      |  "[" Exps "]"         [strict]
                      | ComplexExp "[..]"
 
  syntax AssertEq ::= "assert_eq!" "(" Exp "," Exp ")" ";"  [strict]
                   |  "assert_eq!" "(" Exp "," Exp "," ")" ";"  [strict]
                   |  "assert_eq!" "(" "&" Exp "," "&" Exp ")" ";"  [strict]
                   |  "assert_eq!" "(" ComplexExp "," ComplexExp ")" ";"
                   |  "assert_ne!" "(" Exp "," Exp ")" ";"  [strict]
                   |  "assert_ne!" "(" Exp "," Exp "," ")" ";"  [strict]
                   |  "assert_ne!" "(" "&" Exp "," "&" Exp ")" ";"  [strict]
                   |  "assert_ne!" "(" ComplexExp "," ComplexExp ")" ";"
                  
  syntax Assert  ::= "assert!" "(" Exp ")" ";"  [strict]
                  | "assert!" "("  "(" Exp ")" ")" ";"  [strict]
  syntax Panic ::= "panic!" "(" ")" ";"
  

  syntax RefExp ::= Ref Exp
  syntax RefExps ::= List{RefExp,","}

  syntax SpecialDec ::= "let" Id "=" Id "=" Exp ";"                 [strict(3)]
                      |  "let" "mut" Id "=" Id "=" Exp ";"          [strict(3)]
                      |  "let" Id ":" Type "=" Id "=" Exp ";"       [strict(4)]
                      |  "let" "mut" Id ":" Type "=" Id "=" Exp ";" [strict(4)]
                      |  "let" Id "=" AugAssign ";"
                      |  "let" Id ":" Type "=" AugAssign ";"
                      |  "let" "mut" Id "=" AugAssign ";"
                      |  "let" "mut" Id ":" Type "=" AugAssign ";"
                      | "let" "(" Ids ")" "=" "(" Exps ")" ";"    [strict(2)]
                      |  "let" "(" RefExps ")" "=" "(" RefExps ")" ";"
                      | "let" "()" "=" "()" ";"
                      | "let" "(" "()" "," "()" ")" "=" "(" "()" "," "()" ")" ";" 
                      | "let" StructElementsAccess "=" Exp ";"  [strict(2)]
                      | "let" Id ":" Type "=" Block ";" 
                      | "let" Id "=" Block ";" 
                      | "let" Id "=" "{" StructInstance "}" ";"
                      | "let" Id "=" If ";" 
                      | "let" Id ":" Type "=" If ";" 

  
  syntax ConstAndStatic ::= "const" Id ":" Type "=" Exp ";"  [strict(3)]
                         |  "const" Id ":" Type "=" Ref Exp ";"
                         |  "static" Id ":" Type "=" Exp ";"  [strict(3)]
                         |  "static" "mut" Id ":" Type "=" Exp ";" [strict(3)] 
                         | "const" Function

  syntax Function ::=  "fn" Id "(" TypedIds ")" Body
                  |    "fn" Id "(" TypedIds ")" "->" Type Body   
                  |    "pub" "fn" Id "(" TypedIds ")" "->" Type Body
                  |    "pub" "fn" Id "(" TypedIds ")" Body
                  |    "fn" Id "(" TypedIds ")" "->" "!" Body
                  |    "pub" "fn" Id "(" TypedIds ")" "->" "!" Body

  syntax Closure ::= "|" TypedIds "|" "->" Type Body   //simple closure
                  |  "|" Ids "|" Body    [strict(2)]
                 

  syntax DecClosure ::= "let" Id "=" Closure ";"
                      | "let" "mut" Id "=" Closure ";"
                      | "let" Id "=" "{" Closure "}" ";"
                      | "let" "mut" Id "=" "{" Closure "}" ";"

  syntax Use ::= "use" "std::ops::FnMut" ";"
  syntax SpecStmts ::= "[" Exp ";" Exp "]" ";"
                    |   "format!" "(" String "," Exps ")" ";"  

  syntax Vector ::= "let" "mut" Id "=" "vec!" "[" Exps "]"  ";"    [strict(2)] // vec![1,2,3] 
                  | "let" "mut" Id ":" Type "=" "vec!" "[" Exps "]"  ";"    [strict(2)] 
                  | "let" Id "=" "vec!" "[" Exps "]" ";"  [strict(2)]
                  | "let" Id ":" Type "=" "vec!" "[" Exps "]" ";"  [strict(3)]
                  | Id "=" "vec!" "[" Exps "]" ";"  [strict(2)]
                  | "let" "mut" Id ":" Type "=" "Vec::new()" ";"
  syntax Stmts ::= DeclExp 
                | TypedDecExp 
                | ArrayDec
                | AssignStmt
                | SpecialDec
                | RefStmt
                | Block
                | AugAssign ";"
                | Exp ";"                            [strict]
                | IfAndWhile                          
                | IfAndWhile ";"
                | Function
                | "return" Exp ";"                   [strict]
                | "return" ";"
                | For
                | Attribute
                | Comment
                | PrintStmt
                | Panic
                | AssertEq 
                | Assert 
                | Struct
                | ConstAndStatic
                | Use
                | DecClosure
                | SpecStmts
                | Vector 
                > Stmts Stmts                        [left]
				

endmodule